// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenu.h"

#include "UObject/ConstructorHelpers.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"

#include "Components/TextBlock.h"

#include "ServerField.h"


UMainMenu::UMainMenu(const FObjectInitializer& ObjectInitializer)// : Super(ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> ServerFieldBPClass(TEXT("/Game/UI/ServerFieldWidget"));	//retrieves UI widget
	if (!ensure(ServerFieldBPClass.Class != nullptr)) return;

	ServerFieldClass = ServerFieldBPClass.Class;	//allows code to reference UI widget
}

bool UMainMenu::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	UE_LOG(LogTemp, Warning, TEXT("Menu initialised!"));

	if (!ensure(HostButton != nullptr)) return false;
	HostButton->OnClicked.AddDynamic(this, &UMainMenu::hostGame);	//runs "hostGame" when "HostButton" is clicked

	if (!ensure(JoinButton != nullptr)) return false;
	JoinButton->OnClicked.AddDynamic(this, &UMainMenu::joinGame);	//runs "joinGame" when "JoinButton" is clicked

	if (!ensure(SearchButton != nullptr)) return false;
	SearchButton->OnClicked.AddDynamic(this, &UMainMenu::searchGame);	//runs "searchGame" when "SearchButton" is clicked

	return true;
}

void UMainMenu::setServerList(TArray<FString> serverNames)	//creates an array of strings that store visible servers
{
	UWorld* world = this->GetWorld();
	if (!ensure(world != nullptr)) return;

	ServerList->ClearChildren();	//removes existing fields on scroll box

	uint32 i = 0;
	for (const FString& serverName : serverNames)	//repeats for every server name in given list
	{
		UServerField* field = CreateWidget<UServerField>(world, ServerFieldClass);
		if (!ensure(field != nullptr)) return;

		field->ServerName->SetText(FText::FromString(serverName));
		field->setup(this, i);	//increments with each iteration of the loop
		i++;

		ServerList->AddChild(field);	//adds server name to scroll box
	}
}

void UMainMenu::selectIndex(uint32 index)
{
	selIndex = index;
	UE_LOG(LogTemp, Warning, TEXT("Selected index %d"), selIndex.GetValue());
}

void UMainMenu::hostGame()
{
	UE_LOG(LogTemp, Warning, TEXT("Hosting Game!"));
	if (menuInterface != nullptr)
	{
		menuInterface->host();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Menu interface not set"));
	}
}

/*void UMainMenu::joinGame()
{
	if (menuInterface != nullptr)
	{
		if (selIndex.IsSet())
		{
			UE_LOG(LogTemp, Warning, TEXT("Selected index %d"), selIndex.GetValue());

			menuInterface->join(selIndex.GetValue());
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No server selected"));
		}
		
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Menu interface not set"));
	}
}*/

void UMainMenu::joinGame()
{
	if (selIndex.IsSet())
	{
		UE_LOG(LogTemp, Warning, TEXT("Selected index %d"), selIndex.GetValue());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No server selected"));
	}

	if (menuInterface != nullptr)
	{
		menuInterface->join(selIndex.GetValue());
	}
}

void UMainMenu::searchGame()
{
	if (menuInterface != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Searching for Games!"));
		menuInterface->refreshServerList();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Menu interface not set"));
	}
}