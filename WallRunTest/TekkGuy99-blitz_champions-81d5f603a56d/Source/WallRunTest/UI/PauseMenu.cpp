// Fill out your copyright notice in the Description page of Project Settings.


#include "PauseMenu.h"
#include "Components/Button.h"


bool UPauseMenu::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	UE_LOG(LogTemp, Warning, TEXT("Pause menu initialised!"));

	if (!ensure(ResumeGameButton != nullptr)) return false;
	ResumeGameButton->OnClicked.AddDynamic(this, &UPauseMenu::resumeGame);

	if (!ensure(QuitTitleButton != nullptr)) return false;
	QuitTitleButton->OnClicked.AddDynamic(this, &UPauseMenu::quitGameTitle);

	return true;
}

void UPauseMenu::resumeGame()
{
	removeMenu();
}

void UPauseMenu::quitGameTitle()
{
	if (menuInterface != nullptr)
	{
		removeMenu();
		menuInterface->loadMainMenu();
	}
}