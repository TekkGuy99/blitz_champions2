// Fill out your copyright notice in the Description page of Project Settings.


#include "MenuWidget.h"

void UMenuWidget::setMenuInterface(IMenuInterface* menuInterface)
{
	this->menuInterface = menuInterface;
}

void UMenuWidget::setup()
{
	this->AddToViewport();	//adds menu to viewport

	UWorld* gameWorld = GetWorld();		//get pointer to game world
	if (!ensure(gameWorld != nullptr)) return;

	APlayerController* playerController = gameWorld->GetFirstPlayerController();	//verify player controller
	if (!ensure(playerController != nullptr)) return;

	FInputModeUIOnly inputMode;	//modifies player input for menu navigation
	inputMode.SetWidgetToFocus(this->TakeWidget());	//focuses on main menu UI widget
	inputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);	//unlocks cursor

	playerController->SetInputMode(inputMode);	//sets new imput mode
	playerController->bShowMouseCursor = true;	//makes cursor visible
}

void UMenuWidget::removeMenu()
{
	this->RemoveFromViewport();

	UWorld* gameWorld = GetWorld();		//get pointer to game world
	if (!ensure(gameWorld != nullptr)) return;

	APlayerController* playerController = gameWorld->GetFirstPlayerController();	//verify player controller
	if (!ensure(playerController != nullptr)) return;

	FInputModeGameOnly inputMode;
	playerController->SetInputMode(inputMode);

	playerController->bShowMouseCursor = false;
}