// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ServerField.generated.h"

/**
 * 
 */
UCLASS()
class WALLRUNTEST_API UServerField : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* ServerName;

	void setup(class UMainMenu* parent, uint32 index);	//sets up data for a record field

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* SelectServerButton;

	UFUNCTION()
		void OnClicked();

	UPROPERTY()
		UMainMenu* parent;

	uint32 index;
};
