// Fill out your copyright notice in the Description page of Project Settings.


#include "BlitzGameInstance.h"
#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/PlayerController.h"

#include "Blueprint/UserWidget.h"
#include "UI/MainMenu.h"
#include "UI/MenuWidget.h"

#include "OnlineSessionSettings.h"


//======================================== Initialisation ========================================


const static FName sessionName = TEXT("TestSession");

UBlitzGameInstance::UBlitzGameInstance(const FObjectInitializer& ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/UI/MainMenuWidget"));	//retrieves UI widget
	if (!ensure(MenuBPClass.Class != nullptr)) return;
	MenuClass = MenuBPClass.Class;	//allows code to reference UI widget

	ConstructorHelpers::FClassFinder<UUserWidget> PauseMenuBPClass(TEXT("/Game/UI/PauseMenuWidget"));
	if (!ensure(PauseMenuBPClass.Class != nullptr)) return;
	PauseMenuClass = PauseMenuBPClass.Class;


	UE_LOG(LogTemp, Warning, TEXT("Game Instance Constructor"));
}

void UBlitzGameInstance::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("Found class %s"), *MenuClass->GetName());	//retrieves name of the menu class
	IOnlineSubsystem* subsystem = IOnlineSubsystem::Get();	//sets subsystem

	if (subsystem != nullptr)	//if subsystem has value
	{
		UE_LOG(LogTemp, Warning, TEXT("Found subsystem %s"), *subsystem->GetSubsystemName().ToString());	//print to log
		sessionInterface = subsystem->GetSessionInterface();	//get session interface

		if (sessionInterface.IsValid())		//if interface has value
		{
			UE_LOG(LogTemp, Warning, TEXT("Found session interface"));	//print to log

			sessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UBlitzGameInstance::onCreateSession);		//runs "onCreateSession" when session is created
			sessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UBlitzGameInstance::onDestroySession);		//runs "onDestroySession" when session is destroyed
			sessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UBlitzGameInstance::onFindSession);		//runs "onFindSession" when session is found
			sessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UBlitzGameInstance::onJoinSession);		//runs "onJoinSession" when session is joined
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No subsystem found"));
	}

	UE_LOG(LogTemp, Warning, TEXT("Testing new VS loc"));
}


//======================================== Loading Menus ========================================


void UBlitzGameInstance::loadMenu()		//loads the main menu, runs when entering "MainMenuBG" level
{
	UE_LOG(LogTemp, Warning, TEXT("Loading menu!"));
	if (!ensure(MenuClass != nullptr)) return;

	menu = CreateWidget<UMainMenu>(this, MenuClass);	//originally "UUserWidget"
	if (!ensure(menu != nullptr)) return;


	menu->setup();

	menu->setMenuInterface(this);
}

void UBlitzGameInstance::loadPauseMenu()	//loads the pause menu
{
	UE_LOG(LogTemp, Warning, TEXT("Pausing game"));
	if (!ensure(PauseMenuClass != nullptr)) return;

	UMenuWidget* menu = CreateWidget<UMenuWidget>(this, PauseMenuClass);	//originally "UUserWidget"
	if (!ensure(menu != nullptr)) return;


	menu->setup();

	menu->setMenuInterface(this);
}

void UBlitzGameInstance::loadMainMenu()		//returns player to the main menu
{
	UE_LOG(LogTemp, Warning, TEXT("Returning to title"));
	if (!ensure(PauseMenuClass != nullptr)) return;

	APlayerController* playerController = GetFirstLocalPlayerController();	//verify player controller
	if (!ensure(playerController != nullptr)) return;

	playerController->ClientTravel("/Game/UI/MainMenuBG?listen", ETravelType::TRAVEL_Absolute);	//loadMenu() runs automatically when level loads
}


//======================================== Hosting Sessions ========================================


void UBlitzGameInstance::host()	//hosting a multiplayer session
{
	if (menu != nullptr)
	{
		menu->removeMenu();	//removes any menu, if it is currently open
	}

	if (sessionInterface.IsValid())		//if the session interface has been created
	{
		auto existingSession = sessionInterface->GetNamedSession(sessionName);
		if (existingSession != nullptr)		//if a session already exists
		{
			UE_LOG(LogTemp, Warning, TEXT("Existing session found"));
			sessionInterface->DestroySession(sessionName);	//destroy existing session
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No existing session found"));
			createSession();	//create new session
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Session interface invalid"));
	}
}

void UBlitzGameInstance::onDestroySession(FName sessionName, bool success)	//runs when session is destroyed
{
	if (success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Destroyed existing session"));
		createSession();	//create new session
	}
}

void UBlitzGameInstance::createSession()	//creating a new multiplayer session
{
	if (sessionInterface.IsValid())		//if the session interface has been created
	{
		FOnlineSessionSettings settings;

		settings.bIsLANMatch = true;		//game is only accessible via LAN
		settings.NumPublicConnections = 2;	//game can have 2 public connections, no password required
		settings.bShouldAdvertise = true;	//game is visible when browsing connections

		sessionInterface->CreateSession(0, sessionName, settings);	//creates online session
	}
}

void UBlitzGameInstance::onCreateSession(FName sessionName, bool success)	//runs when session is created
{
	if (!success)	//if function fails
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't create session"));
		return;
	}

	UEngine* engine = GetEngine();	//verify engine
	if (!ensure(engine != nullptr)) return;

	engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Now Hosting..."));

	UWorld* world = GetWorld();		//verify game world
	if (!ensure(engine != nullptr)) return;

	world->ServerTravel("/Game/ThirdPersonBP/Maps/BlitzTestArea?listen");	//send all players to new level
}


//======================================== Joining Sessions ========================================


void UBlitzGameInstance::refreshServerList()
{
	sessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (sessionSearch.IsValid())
	{
		/*sessionSearch->bIsLanQuery = true;
		sessionSearch->MaxSearchResults = 100;	//NEW - shortens maximum returned list of sessions
		sessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);	//NEW - sets parameters to search for online sessions*/

		UE_LOG(LogTemp, Warning, TEXT("Beginning session search"));

		//TArray<FString> serverNames;

		sessionInterface->FindSessions(0, sessionSearch.ToSharedRef());
	}
}

void UBlitzGameInstance::onFindSession(bool success)
{
	UEngine* engine = GetEngine();	//verify engine
	if (!ensure(engine != nullptr)) return;

	if (success && sessionSearch.IsValid() && menu != nullptr)	//if function is successful AND menu is valid
	{
		if (sessionSearch->SearchResults.Num() < 1)
		{
			UE_LOG(LogTemp, Warning, TEXT("No existing sessions found"));
			engine->AddOnScreenDebugMessage(0, 2, FColor::Orange, TEXT("No existing sessions found"));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("%d session(s) found"), sessionSearch->SearchResults.Num());
			TArray<FString> serverNames;

			for (const FOnlineSessionSearchResult& SearchResult : sessionSearch->SearchResults)		//for every session found
			{
				UE_LOG(LogTemp, Warning, TEXT("Found session: %s"), *SearchResult.GetSessionIdStr());
				engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Found session"));//: %s"), *SearchResult.GetSessionIdStr());

				serverNames.Add(SearchResult.GetSessionIdStr());
			}

			menu->setServerList(serverNames);
		}
	}
	/*else
	{
		UE_LOG(LogTemp, Warning, TEXT("Couldn't find session"));
		return;
	}*/
}

void UBlitzGameInstance::join(uint32 index)	//user inputs ip address
{
	if (!sessionInterface.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Session interface not found"));
		return;
	}
	if (!sessionSearch.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Session search invalid"));
		return;
	}

	if (menu != nullptr)
	{
		menu->removeMenu();	//removes main menu, if it is currently open
	}

	sessionInterface->JoinSession(0, sessionName, sessionSearch->SearchResults[index]);
}

void UBlitzGameInstance::onJoinSession(FName sessionName, EOnJoinSessionCompleteResult::Type result)
{
	if (!sessionInterface.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Session interface not found"));
		return;
	}

	FString address;

	if (!sessionInterface->GetResolvedConnectString(sessionName, address))
	{
		UE_LOG(LogTemp, Warning, TEXT("Could not get connect string"));
		return;
	}

	UEngine* engine = GetEngine();	//verify engine
	if (!ensure(engine != nullptr)) return;

	engine->AddOnScreenDebugMessage(0, 2, FColor::Green, FString::Printf(TEXT("Now Joining %s..."), *address));

	APlayerController* playerController = GetFirstLocalPlayerController();	//verify player controller
	if (!ensure(playerController != nullptr)) return;

	//playerController->ClientTravel("/Game/ThirdPersonBP/Maps/BlitzTestArea?listen", ETravelType::TRAVEL_Absolute);
	playerController->ClientTravel(address, ETravelType::TRAVEL_Absolute);	//travel to server
}